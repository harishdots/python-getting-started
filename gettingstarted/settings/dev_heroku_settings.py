from gettingstarted.settings.base import *
import dj_database_url

db_from_env = dj_database_url.config(conn_max_age=500)

DEBUG = True
DATA_UPLOAD_MAX_NUMBER_FIELDS = 3000
# for local dev:
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', ''),#'d6kcnqacr8q1j4',
        'USER': os.environ.get('DATABASE_USER', ''),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        #'HOST': 'localhost',
        'HOST':os.environ.get('DATABASE_HOST', ''),
        'PORT': '',
    }
}

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

ALLOWED_HOSTS = [
    '0.0.0.0', 'localhost', 'python-demo-staging.herokuapp.com', '127.0.0.1'
]

INTERNAL_IPS = (
    '10.0.2.2'
    )
# for debug toolbar / vagrant port found with request.META['REMOTE_ADDR']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO')
        },
        'django.template': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    },
}